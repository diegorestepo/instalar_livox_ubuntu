#
#  ------------------------------------------------------------------------------------
#  "THE BEER-WARE LICENSE" (Revision 42):
#  <diegorestrepoleal@gmail.com> wrote this file. As long as you retain this notice you
#  can do whatever you want with this stuff. If we meet some day, and you think
#  this stuff is worth it, you can buy me a beer in return Diego Andrés Restrepo Leal.
#  ------------------------------------------------------------------------------------
#


#!/bin/bash


clear

AZUL=$(tput setaf 6)
VERDE=$(tput setaf 2)
ROJO=$(tput setaf 1)
LILA=$(tput setaf 5)
LIMPIAR=$(tput sgr 0)


echo
echo "$VERDE INICIO $LIMPIAR"
echo


echo
echo "$AZUL Ingrese el usuario $LIMPIAR"
echo
read usuario


echo
echo "$AZUL Actualizar Ubuntu $LIMPIAR"
echo
sudo apt update -y
sudo apt install -y build-essential
sudo apt install -y git curl wget

echo
echo "$AZUL Instalar ROS - Melodic $LIMPIAR"
echo
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
sudo apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654
curl -sSL 'http://keyserver.ubuntu.com/pks/lookup?op=get&search=0xC1CF6E31E6BADE8868B172B4F42ED6FBAB17C654' | sudo apt-key add -
sudo apt update -y
sudo apt install -y ros-melodic-desktop-full
echo "source /opt/ros/melodic/setup.bash" >> /home/$usuario/.bashrc
source ~/.bashrc
sudo apt install -y python-rosdep python-rosinstall python-rosinstall-generator python-wstool build-essential
sudo apt install -y python-rosdep
sudo rosdep init
rosdep update


echo
echo "Instalar PCL"
echo
sudo apt install -y libpcl-dev


echo
echo "Instalar Eigen"
echo
sudo apt install -y libeigen3-dev


echo
echo "Instalar OpenCV"
echo
sudo apt install -y libopencv-dev


echo
echo "livox_ros_driver"
echo
git clone https://github.com/Livox-SDK/livox_ros_driver.git ws_livox/src
cd ws_livox
source /home/$usuario/.bashrc
catkin_make
#source ./devel/setup.sh
#echo "source /opt/ros/melodic/setup.bash" >> ~/.bashrc

exit 0
