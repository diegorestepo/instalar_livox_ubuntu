#!/bin/bash

cd /home/ubuntu/LIVOX_DATA/

fecha=$(date)
fecha=`echo $fecha | tr ' ' '_'`
mkdir respaldo_"$fecha"
mv *.pcd *.txt respaldo_"$fecha"

exit 0
