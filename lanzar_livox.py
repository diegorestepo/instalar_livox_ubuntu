#!/usr/bin/python3


import subprocess
from time import sleep


def run_livox():
    print('Livox 1')
    subprocess.run(['bash', '/home/ubuntu/lanzar_livox_1.sh'])
    sleep(20)
    print('Livox 2')
    subprocess.run(['bash', '/home/ubuntu/lanzar_livox_2.sh'])


def leer_PID():
    with open('/home/ubuntu/LIVOX_DATA/pid_livox_1.txt', 'r') as f:
        for line in f:
            for word in line.split():
                pid_livox_1 = word

    with open('/home/ubuntu/LIVOX_DATA/pid_livox_2.txt', 'r') as f:
        for line in f:
            for word in line.split():
                pid_livox_2 = word

    return pid_livox_1, pid_livox_2


def respaldar_datos():
    subprocess.run(['bash', '/home/ubuntu/respaldo.sh'])


def matar_proceso(pid_livox_1, pid_livox_2):
    subprocess.run(['kill', '-15', pid_livox_1])
    subprocess.run(['kill', '-15', pid_livox_2])


def run():
    #VEINTE_MINUTOS = 1200
    #CINCO_MINUTOS = 300
    sleep(10)
    run_livox()
    sleep(300) #Ajustar tiempo de grabación.
    pid_livox_1, pid_livox_2 = leer_PID()
    matar_proceso(pid_livox_1, pid_livox_2)
    sleep(30)
    respaldar_datos()


if __name__ == '__main__':
    run()
